import $ from 'jquery';
import 'bootstrap';
import Rellax from 'rellax';

import './module/clicker';

const apiUrl = 'https://api.openweathermap.org/data/2.5/weather?q=Chongqing,CN&units=metric&appid=a5d4e8150b8875ad5b7493b39c1fdc2f';
const locationName = document.querySelector('.location');
const temp = document.querySelector('.temp');
const desc = document.querySelector('.desc');
let data;
const rellax = new Rellax('.rellax');
const menuTrigger = $('.hamburger-menu');
const closeTrigger = $('.close');
const menu = $('menu');
let menuOpen = false;
const welcome = $('.Home');
const personalInfo = $('.About');
const contactMe = $('.Contact');
const homeClicked = $('.Home');
const aboutClicked = $('.About');
const contactClicked = $('.Contact');
const sencondaryNavTrigger2 = $('.secondary-trigger2');
const sencondaryNav2 = $('.Secondary2');
const gameDesign = $('.Game');
const closeGameDesign = $('.back');
const modelDesign = $('.3D');
const closeModelDesign = $('.back');
const photography = $('.Photo');
const closePhotography = $('.back');

fetch(apiUrl, {
    method: 'get'
})
    .then(response => response.json())
    .then(jsonData => {
        data = jsonData;
        console.log('My weather data: ', data);
        locationName.innerHTML = data.name;
        temp.innerHTML = data.main.temp;
        desc.innerHTML = data.weather[0].description;
    })
    .catch(err => console.log(err));

photography.on('click', function () {
    if (menuOpen === true) {
        menu.removeClass('open-menu');
        menuOpen = false;
    }
    $('.PhotoContainer').addClass('open-Photo');
    $('.blur-bg').addClass('open-Photo');
    $('.PhotoHeader').addClass('open-Photo');
    $('.Homebar').addClass('nav-disappear');
    $('.Aboutbar').addClass('nav-disappear');
    $('.Contactbar').addClass('nav-disappear');
    $('nav').addClass('nav-disappear');
});

closePhotography.on('click', function () {
    $('.PhotoContainer').removeClass('open-Photo');
    $('.blur-bg').removeClass('open-Photo');
    $('.PhotoHeader').removeClass('open-Photo');
    $('.Homebar').removeClass('nav-disappear');
    $('.Aboutbar').removeClass('nav-disappear');
    $('.Contactbar').removeClass('nav-disappear');
    $('nav').removeClass('nav-disappear');
});

closeModelDesign.on('click', function () {
    $('.Container2').removeClass('open-3Dpage');
    $('.blur-bg').removeClass('open-3Dpage');
    $('.ModelHeader').removeClass('open-3Dpage');
    $('.modelInfo').removeClass('open-3Dpage');
    $('.Homebar').removeClass('nav-disappear');
    $('.Aboutbar').removeClass('nav-disappear');
    $('.Contactbar').removeClass('nav-disappear');
    $('nav').removeClass('nav-disappear');
});

modelDesign.on('click', function () {
    if (menuOpen === true) {
        menu.removeClass('open-menu');
        menuOpen = false;
    }
    $('.Container2').addClass('open-3Dpage');
    $('.blur-bg').addClass('open-3Dpage');
    $('.ModelHeader').addClass('open-3Dpage');
    $('.modelInfo').addClass('open-3Dpage');
    $('.Homebar').addClass('nav-disappear');
    $('.Aboutbar').addClass('nav-disappear');
    $('.Contactbar').addClass('nav-disappear');
    $('nav').addClass('nav-disappear');
});

closeGameDesign.on('click', function () {
    $('.video1').removeClass('open-Gamepage');
    $('.Game-info').removeClass('open-Gamepage');
    $('.Container').removeClass('open-Gamepage');
    $('.blur-bg').removeClass('open-Gamepage');
    $('.gameHeader').removeClass('open-Gamepage');
    $('.Homebar').removeClass('nav-disappear');
    $('.Aboutbar').removeClass('nav-disappear');
    $('.Contactbar').removeClass('nav-disappear');
    $('nav').removeClass('nav-disappear');
});

gameDesign.on('click', function () {
    if (menuOpen === true) {
        menu.removeClass('open-menu');
        menuOpen = false;
    }
    $('.video1').addClass('open-Gamepage');
    $('.Game-info').addClass('open-Gamepage');
    $('.Container').addClass('open-Gamepage');
    $('.blur-bg').addClass('open-Gamepage');
    $('.gameHeader').addClass('open-Gamepage');
    $('.Homebar').addClass('nav-disappear');
    $('.Aboutbar').addClass('nav-disappear');
    $('.Contactbar').addClass('nav-disappear');
    $('nav').addClass('nav-disappear');
});

sencondaryNavTrigger2.on('click', function () {
    $(this).toggleClass('rotate2');
    sencondaryNav2.toggleClass('is-hidden2');
});

homeClicked.on('click', function () {
    $('.Homebar').addClass('Click-navbars');
    $('.Aboutbar').removeClass('Click-navbars');
    $('.Contactbar').removeClass('Click-navbars');
    $('.Home').addClass('fontActivated');
    $('.About').removeClass('fontActivated');
    $('.Contact').removeClass('fontActivated');
});

aboutClicked.on('click', function () {
    $('.Contactbar').removeClass('Click-navbars');
    $('.Homebar').removeClass('Click-navbars');
    $('.Aboutbar').addClass('Click-navbars');
    $('.Home').removeClass('fontActivated');
    $('.About').addClass('fontActivated');
    $('.Contact').removeClass('fontActivated');
});

contactClicked.on('click', function () {
    $('.Contactbar').addClass('Click-navbars');
    $('.Aboutbar').removeClass('Click-navbars');
    $('.Homebar').removeClass('Click-navbars');
    $('.Home').removeClass('fontActivated');
    $('.About').removeClass('fontActivated');
    $('.Contact').addClass('fontActivated');
});

menuTrigger.on('click', function () {
    if (menuOpen === false) {
        menu.addClass('open-menu');
        menuOpen = true;
    }
});

closeTrigger.on('click', function () {
    if (menuOpen === true) {
        menu.removeClass('open-menu');
        menuOpen = false;
    }
});

welcome.on('click', function () {
    $('.intro').toggleClass('intro-appear');
    $('.Maskmove-Home').toggleClass('Textmask');
    $('.PersonalInfo').removeClass('Aboutme-appear');
    $('.Myphoto').removeClass('Aboutme-appear2');
    $('.Maskmove-About').removeClass('Aboutme-appear2');
    $('.Contact-details').removeClass('intro-appear');
});

personalInfo.on('click', function () {
    $('.intro').removeClass('intro-appear');
    $('.Maskmove-Home').removeClass('Textmask');
    $('.PersonalInfo').addClass('Aboutme-appear');
    $('.Maskmove-About').addClass('Aboutme-appear2');
    $('.Myphoto').addClass('Aboutme-appear2');
    $('.Contact-details').removeClass('intro-appear');
});

contactMe.on('click', function () {
    $('.PersonalInfo').removeClass('Aboutme-appear');
    $('.Maskmove-About').removeClass('Aboutme-appear2');
    $('.Myphoto').removeClass('Aboutme-appear2');
    $('.intro').removeClass('intro-appear');
    $('.Maskmove-Home').toggleClass('Textmask');
    $('.Contact-details').toggleClass('intro-appear');
});

window.$ = $;
window.jQuery = $;
